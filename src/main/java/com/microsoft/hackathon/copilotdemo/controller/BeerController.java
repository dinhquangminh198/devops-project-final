package com.microsoft.hackathon.copilotdemo.controller;

import com.microsoft.hackathon.copilotdemo.data.Beer;
import com.microsoft.hackathon.copilotdemo.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/beers")
public class BeerController {

    private final BeerService beerService;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }

    // API cho việc lấy danh sách bia với phân trang
    @GetMapping("/pagination")
    public List<Beer> getBeersWithPagination(@RequestParam int offset, @RequestParam int limit) throws IOException {
        return beerService.getBeersWithPagination(offset, limit);
    }

    // API cho việc tìm kiếm bia theo tên, mô tả, tagline, food pairing và giá
    @GetMapping("/search")
    public List<Beer> searchBeers(@RequestParam String searchTerm) throws IOException {
        return beerService.searchBeers(searchTerm);
    }

    @GetMapping("/{id}")
    public Beer getBeerById(@PathVariable int id) throws IOException {
        return beerService.getBeerById(id);
    }
}
