package com.microsoft.hackathon.copilotdemo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")
public class PhoneNumberValidationController {

    private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$");

    @GetMapping("/validate/{phoneNumber}")
    public ResponseEntity<String> validatePhoneNumber(@PathVariable String phoneNumber) {
        if (isValidPhoneNumber(phoneNumber)) {
            return ResponseEntity.ok("Phone number is valid.");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid phone number format.");
        }
    }

    private boolean isValidPhoneNumber(String phoneNumber) {
        return PHONE_NUMBER_PATTERN.matcher(phoneNumber).matches();
    }
}
