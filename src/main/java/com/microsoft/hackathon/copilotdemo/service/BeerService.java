package com.microsoft.hackathon.copilotdemo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.hackathon.copilotdemo.data.Beer;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BeerService {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String filePath = "src/main/java/com/microsoft/hackathon/copilotdemo/storage/database.json"; // Đường dẫn tới tệp JSON

    public List<Beer> getAllBeers() throws IOException {
        return Arrays.asList(objectMapper.readValue(new File(filePath), Beer[].class));
    }

    public Beer getBeerById(int id) throws IOException {
        List<Beer> beers = getAllBeers();
        return beers.stream().filter(beer -> beer.getId() == id).findFirst().orElse(null);
    }

    public List<Beer> getBeersWithPagination(int offset, int limit) throws IOException {
        List<Beer> beers = getAllBeers();
        return beers.stream().skip(offset).limit(limit).collect(Collectors.toList());
    }
    public List<Beer> searchBeers(String searchTerm) throws IOException {
        List<Beer> beers = getAllBeers();
        return beers.stream()
                .filter(beer -> beer.getName().toLowerCase().contains(searchTerm.toLowerCase())
                        || beer.getDescription().toLowerCase().contains(searchTerm.toLowerCase())
                        || beer.getTagline().toLowerCase().contains(searchTerm.toLowerCase())
                        || Arrays.stream(beer.getFoodPairing()).anyMatch(pairing -> pairing.toLowerCase().contains(searchTerm.toLowerCase()))
                        || Double.toString(beer.getPrice()).equals(searchTerm))
                .collect(Collectors.toList());
    }
}
