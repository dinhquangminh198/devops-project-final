package com.microsoft.hackathon.copilotdemo.data;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@AllArgsConstructor
public class Beer {
    private int id;
    private String name;
    private String tagline;
    private String firstBrewed;
    private String description;
    private String imageUrl;
    private double price;
    private double abv;
    private int ibu;
    private int targetFg;
    private int targetOg;
    private int ebc;
    private int srm;
    private double ph;
    private int attenuationLevel;
    private Volume volume;
    private Volume boilVolume;
    private String[] foodPairing;
    private String brewersTips;
    private String contributedBy;

    public Beer(int id, String beer1) {
    }
}

