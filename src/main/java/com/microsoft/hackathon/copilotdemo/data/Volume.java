package com.microsoft.hackathon.copilotdemo.data;

import lombok.Data;

@Data
public class Volume {
    private int value;
    private String unit;

    // Constructors, getters, setters
}
