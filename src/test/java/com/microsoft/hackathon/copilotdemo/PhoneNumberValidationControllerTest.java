package com.microsoft.hackathon.copilotdemo;

import com.microsoft.hackathon.copilotdemo.controller.PhoneNumberValidationController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PhoneNumberValidationControllerTest {

    private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$");

    @Test
    public void testValidPhoneNumber() {
        PhoneNumberValidationController controller = new PhoneNumberValidationController();

        ResponseEntity<String> response = controller.validatePhoneNumber("+1234567890");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Phone number is valid.", response.getBody());
    }

    @Test
    public void testInvalidPhoneNumber() {
        PhoneNumberValidationController controller = new PhoneNumberValidationController();

        ResponseEntity<String> response = controller.validatePhoneNumber("1234567890");

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Invalid phone number format.", response.getBody());
    }

    @Test
    public void testPatternMatching() {
        Assertions.assertTrue(PHONE_NUMBER_PATTERN.matcher("+1234567890").matches());
        Assertions.assertFalse(PHONE_NUMBER_PATTERN.matcher("1234567890").matches());
    }
}
